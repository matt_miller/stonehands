defmodule Stonehands.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    Handiman.Data.fetch_and_load()
    topologies = Application.get_env(:libcluster, :topologies) || []

    children = [
      # Start the PubSub system
      {Phoenix.PubSub, name: Stonehands.PubSub},
      # Start the Endpoint (http/https)
      StonehandsWeb.Endpoint,
      {Cluster.Supervisor, [topologies, [name: Stonehands.ClusterSupervisor]]},
      # Update the predictions every ~30m
      {Stonehands.Updater, 1_819_109}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Stonehands.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    StonehandsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
