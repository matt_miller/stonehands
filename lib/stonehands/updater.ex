defmodule Stonehands.Updater do
  use Task, restart: :permanent
  require Logger

  def start_link(arg) do
    Task.start_link(__MODULE__, :run, [arg])
  end

  def run(ms) do
    Process.sleep(ms)
    Handiman.Data.fetch_and_load()
    Logger.info("Predictions updated")
    run(ms)
  end
end
