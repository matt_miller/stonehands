defmodule Stonehands do
  @moduledoc """
  Stonehands keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  def wager_heat(w, br) when is_number(w) do
    pct = w / br * 100

    cond do
      pct >= 31 -> "bg-emerald-500 dark:bg-green-700"
      pct >= 23 -> "bg-emerald-400 dark:bg-sky-700"
      pct >= 13 -> "bg-emerald-300 dark:bg-emerald-700"
      pct >= 7 -> "bg-emerald-200 dark:bg-cyan-700"
      true -> ""
    end
  end

  def wager_heat(_, _), do: ""

  def wager_table(opts) do
    case Handiman.wager_table(opts) do
      [] -> [{"No positive expectation wagers", "-"}]
      data -> data
    end
  end
end
