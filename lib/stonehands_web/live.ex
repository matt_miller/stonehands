defmodule StonehandsWeb.Live do
  use StonehandsWeb, :live_view
  @about_an_hour 3_599_969
  @follow_sides Application.compile_env(:handiman, :side) |> Keyword.get(:weights)
  @fade_sides @follow_sides |> Enum.map(fn {d, a} -> {d, -1 * a} end)
  @follow_totes Application.compile_env(:handiman, :tote) |> Keyword.get(:weights)
  @fade_totes @follow_totes |> Enum.map(fn {d, a} -> {d, -1 * a} end)

  @defaults %{
    :roll => 1000,
    :count => 16,
    :pct => 2.5,
    :ot => :us,
    :ov => "-110",
    :side => @follow_sides,
    :tote => @follow_totes
  }
  def mount(_params, _session, socket) do
    if connected?(socket), do: Process.send_after(self(), :refresh, @about_an_hour)

    opts = [
      bankroll: @defaults[:roll],
      max_count: @defaults[:count],
      min_pct: @defaults[:pct],
      offered: [{@defaults[:ot], @defaults[:ov]}],
      side: @defaults[:side],
      tote: @defaults[:tote]
    ]

    {:ok,
     assign(socket,
       opts: opts,
       model: "follow",
       ot: :us,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  def render(assigns) do
    ~L"""
    <section class="phx-hero" id="page-live" phx-hook="weback">
    <div class="mx-2 grid grid-cols-1 md:grid-cols-4 gap-10 justify-center font-mono">

    <div>
    <%= for {{rt,rs}, {ht,hs}} <- @outcomes do %>
      <table class="my-4">
      <tr class="<%= if rs >= hs, do: "bg-sky-500 dark:bg-sky-700" %>"><td width="90%"><%= rt %></td><td align="right"><%= rs %></td></tr>
      <tr class="<%= if hs >= rs, do: "bg-sky-500 dark:bg-sky-700" %>"><td width="90%"><%= ht %></td><td align="right"><%= hs %></td></tr>
      </table>
    <% end %>
    </div>
    <div class="col-span-2">
      <table> 
      <thead>
    <tr>
      <th>Matchup</th>
      <th>Wager</th>
    </tr>
    </thead>
    <tbody>
    <%= for {m,w} <- @wagers do %>
    <tr class="<%= Stonehands.wager_heat(w, @opts[:bankroll]) %>">
        <td width="50%"><%= m %></td>
    <td width="50%" align="right"><%= w %></td>
      </tr>
    <% end %>
    </tbody>
    </table>
    </div>
    <div>
    <table>
    <tr><th align="center" colspan=2>Wager Options</th></tr>
    <tr><td>
    <table>
    <tr><td>Model:</td>
    <td><form id="mod-form" phx-change="mod-change" phx-submit="mod-change" phx-hook="modelUpdated">
    <select name="model" id="select_mod" class="bg-white dark:bg-black">
        <%= for m <- ["follow","fade"] do %>
          <option value="<%= m %>"  <%= if @model == m,  do: "selected" %>>
            <%= m  %>
          </option>
        <% end %>
      </select>
    </form></td></tr>
    <tr><td>Offered Odds:</td>
     <td><input class="bg-white dark:bg-black" size="6" id="odds" pattern="[0-9\+\.\-]*" type="text" phx-blur="odds_string" phx-hook="oddsvalUpdated" value="<%= @opts[:offered] |> List.first |> elem(1) %>">
    <td><form id="ot-form" phx-change="ot-change" phx-submit="ot-change" phx-hook="oddstypeUpdated">
    <select name="odds_type" id="select_ot" class="bg-white dark:bg-black">
        <%= for {o,n} <- [us: "ML", eu: "Dec", hk: "HK"] do %>
          <option value="<%= o %>"  <%= if Keyword.has_key?(@opts[:offered],o), do: "selected" %>>
            <%= n  %>
          </option>
        <% end %>
      </select>
    </form></td></tr>
    <tr><td>Bankroll:</td>
     <td><input class="bg-white dark:bg-black" size="6" id="bankroll" type="text" phx-blur="bankroll" phx-hook="rollUpdated" inputmode="numeric" pattern="[0-9]*" value="<%= @opts[:bankroll] %>">
    </td></tr>
    <tr><td>Max count:</td>
    <td><input class="bg-white dark:bg-black" size="3" id="count" type="text" phx-blur="count" phx-hook="countUpdated" inputmode="numeric" pattern="[0-9]*" value="<%= @opts[:max_count] %>">
    </td></tr>
    <tr><td> Min percent:</td>
    <td><input class="bg-white dark:bg-black" size="6" id="pct" type="text" phx-blur="pct" phx-hook="pctUpdated" inputmode="numeric" pattern="[0-9]*" value="<%= @opts[:min_pct] %>">
    </td></tr>
    </table>
    </div>
    </div>
    """
  end

  def handle_event(
        "weback",
        %{
          "count" => c,
          "odds_type" => ot,
          "odds_val" => ov,
          "pct" => p,
          "roll" => r,
          "model" => m
        },
        socket
      ) do
    opts =
      [
        bankroll: option(r, @defaults[:roll], fn s -> String.to_integer(s) end),
        max_count: option(c, @defaults[:count], fn s -> String.to_integer(s) end),
        min_pct:
          option(p, @defaults[:pct], fn s ->
            {cent, _} = Float.parse(s)
            cent
          end),
        offered: [
          {option(ot, @defaults[:ot], fn o -> String.to_existing_atom(o) end),
           option(ov, @defaults[:ov], fn s -> s end)}
        ]
      ]
      |> model_opts(m)

    {:noreply,
     assign(socket,
       opts: opts,
       model: m,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  def handle_event("bankroll", %{"value" => brs}, socket) do
    opts =
      Keyword.put(
        socket.assigns.opts,
        :bankroll,
        String.to_integer(brs)
      )

    {:noreply,
     assign(push_event(socket, "rollUpdated", %{roll: brs}),
       opts: opts,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  def handle_event("mod-change", %{"model" => ms}, socket) do
    opts = model_opts(socket.assigns.opts, ms)

    {:noreply,
     assign(push_event(socket, "modelUpdated", %{val: ms}),
       opts: opts,
       model: ms,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  def handle_event("ot-change", %{"odds_type" => os}, socket) do
    {old_atom, old_value} = socket.assigns.opts[:offered] |> List.first()
    new_atom = String.to_existing_atom(os)
    new_val = Exoddic.convert(old_value, from: old_atom, to: new_atom)

    opts =
      Keyword.put(
        socket.assigns.opts,
        :offered,
        [{new_atom, new_val}]
      )

    {:noreply, assign(push_event(socket, "oddstypeUpdated", %{type: os}), opts: opts)}
  end

  def handle_event("odds_string", %{"value" => os}, socket) do
    {old_atom, _} = socket.assigns.opts[:offered] |> List.first()

    opts =
      Keyword.put(
        socket.assigns.opts,
        :offered,
        [{old_atom, os}]
      )

    {:noreply,
     assign(push_event(socket, "oddsvalUpdated", %{val: os}),
       opts: opts,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  def handle_event("count", %{"value" => cs}, socket) do
    opts =
      Keyword.put(
        socket.assigns.opts,
        :max_count,
        String.to_integer(cs)
      )

    {:noreply,
     assign(push_event(socket, "countUpdated", %{count: cs}),
       opts: opts,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  def handle_event("pct", %{"value" => ps}, socket) do
    {flt, _} = Float.parse(ps)

    opts = Keyword.put(socket.assigns.opts, :min_pct, flt)

    {:noreply,
     assign(push_event(socket, "pctUpdated", %{pct: flt}),
       opts: opts,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  def handle_info(:refresh, socket) do
    Process.send_after(self(), :refresh, @about_an_hour)

    Handiman.Data.load([:side, :tote])
    opts = socket.assigns.opts

    {:noreply,
     assign(socket,
       outcomes: Handiman.pred_outcome_table(opts),
       wagers: Stonehands.wager_table(opts)
     )}
  end

  defp model_opts(opts, "fade"),
    do: opts |> Keyword.put(:side, @fade_sides) |> Keyword.put(:tote, @fade_totes)

  defp model_opts(opts, "follow"),
    do: opts |> Keyword.put(:side, @follow_sides) |> Keyword.put(:tote, @follow_totes)

  defp model_opts(opts, _), do: model_opts(opts, "follow")

  defp option(nil, default, _fun), do: default
  defp option(val, _default, fun), do: fun.(val)
end
