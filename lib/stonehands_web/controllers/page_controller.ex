defmodule StonehandsWeb.PageController do
  use StonehandsWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html",
      wagers: Handiman.wager_table(bankroll: 1000, min_pct: 5, max_count: 5)
    )
  end
end
