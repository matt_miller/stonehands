import Config

# For production, don't forget to configure the url host
# to something meaningful, Phoenix uses this information
# when generating URLs.
#
# Note we also include the path to a cache manifest
# containing the digested version of static files. This
# manifest is generated by the `mix phx.digest` task,
# which you should run after static files are built and
# before starting your production server.
config :stonehands, StonehandsWeb.Endpoint,
  url: [host: "https://stonehands.zebrine.net"],
  check_origin: [
    "https://stonehands.zebrine.net",
    "https://stonehands.fly.dev",
    "https://www.zebrine.net",
    "http://stonehands.zebrine.net",
    "http://www.zebrine.net",
    "http://stonehands.fly.dev"
  ],
  cache_static_manifest: "priv/static/cache_manifest.json"

# Do not print debug messages in production
config :logger, level: :error

config :handiman,
  side: [
    csv: "/var/tmp/sides.csv",
    uri: "https://www.thepredictiontracker.com/nflpredictions.csv",
    weights: [
      {"midweek", 3},
      {"l2to", 2},
      {"moore", 2},
      {"l2h", 1},
      {"l2", 1},
      {"dok", 1},
      {"ca", 1},
      {"open", 2},
      {"round", -2}
    ]
  ],
  tote: [
    csv: "/var/tmp/totes.csv",
    uri: "https://www.thepredictiontracker.com/nfltotals.csv",
    weights: [
      {"midweek", 5},
      {"ffw", 2},
      {"talis", 2},
      {"donchess", 2},
      {"open", 1},
      {"argh", -1}
    ]
  ]
